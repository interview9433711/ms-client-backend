package com.nttdata;

import com.nttdata.domain.model.gateway.ClientRepository;
import com.nttdata.domain.model.gateway.IdentificationTypeRepository;
import com.nttdata.domain.usecases.client.ClientUseCase;
import com.nttdata.domain.usecases.identification.IdentificationTypeUseCase;
import com.nttdata.infrastructure.drivenadapters.mappers.client.ClientMapper;
import com.nttdata.infrastructure.drivenadapters.mappers.client.ClientMapperImpl;
import com.nttdata.infrastructure.drivenadapters.mappers.identification.IdentificationTypeMapper;
import com.nttdata.infrastructure.drivenadapters.mappers.identification.IdentificationTypeMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    public IdentificationTypeUseCase identificationTypeUseCase(IdentificationTypeRepository identificationTypeRepository) {
        return new IdentificationTypeUseCase(identificationTypeRepository);
    }

    @Bean
    public ClientUseCase clientUseCase(ClientRepository clientRepository, IdentificationTypeUseCase identificationTypeUseCase) {
        return new ClientUseCase(clientRepository, identificationTypeUseCase);
    }

    @Bean
    public ClientMapper clientMapper() {
        return new ClientMapperImpl();
    }

    @Bean
    public IdentificationTypeMapper identificationTypeMapper() {
        return new IdentificationTypeMapperImpl();
    }
}
