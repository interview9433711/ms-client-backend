package com.nttdata.defaults;

import com.nttdata.domain.model.client.Client;
import com.nttdata.domain.model.gateway.ClientRepository;
import com.nttdata.domain.model.gateway.IdentificationTypeRepository;
import com.nttdata.domain.model.identification.IdentificationType;
import lombok.extern.java.Log;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

@Log
@Configuration
public class DefaultBeansConfig {
    private final ClientRepository clientRepository = new ClientRepository() {
        @Override
        public Client saveClient(Client client) {
            return Client.builder().build();
        }

        @Override
        public Client searchClientByIdentification(String identification) {
            return Client.builder().build();
        }

        @Override
        public boolean deleteClientByIdentification(String identification) {
            return false;
        }

        @Override
        public Client updateClient(String identification, Client client) {
            return Client.builder().build();
        }

        @Override
        public List<Client> findAllClients() {
            return Collections.emptyList();
        }

        @Override
        public Client searchClientByIdentificationTypeAndIdentificationNumber(String identificationType, String identificationNumber) {
            return Client.builder().build();
        }
    };
    private final IdentificationTypeRepository identificationTypeRepository = new IdentificationTypeRepository() {
        @Override
        public IdentificationType searchIdentificationTypeByCode(String code) {
            return IdentificationType.builder().build();
        }

        @Override
        public List<IdentificationType> finAllIdentificationType() {
            return Collections.emptyList();
        }
    };

    @Bean
    @ConditionalOnMissingBean
    public ClientRepository clientRepository() {
        alertFakeBean("ClientRepository");
        return clientRepository;
    }

    @Bean
    @ConditionalOnMissingBean
    public IdentificationTypeRepository identificationTypeRepository() {
        alertFakeBean("IdentificationTypeRepository");
        return identificationTypeRepository;
    }

    private void alertFakeBean(String beanName) {
        log.log(Level.WARNING, "CONFIGURATION FAKE " + beanName, beanName);
    }
}
