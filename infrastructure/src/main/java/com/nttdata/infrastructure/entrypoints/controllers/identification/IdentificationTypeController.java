package com.nttdata.infrastructure.entrypoints.controllers.identification;

import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.domain.usecases.identification.IdentificationTypeUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/identification", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class IdentificationTypeController {

    private final IdentificationTypeUseCase identificationTypeUseCase;

    @GetMapping
    public List<IdentificationType> findAll() {
        return identificationTypeUseCase.findAllIdentificationType();
    }

    @GetMapping("/type")
    public IdentificationType searchById(@RequestParam(value = "code", required = true) String code) {
        return identificationTypeUseCase.findByIdentificationCode(code);
    }
}
