package com.nttdata.infrastructure.entrypoints.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class MessageResponse {

    @JsonProperty(value = "mensaje")
    private String message;

    @JsonProperty(value = "fecha")
    private LocalDateTime date;

}
