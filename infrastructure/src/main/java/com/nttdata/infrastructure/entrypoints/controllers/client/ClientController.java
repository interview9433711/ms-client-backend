package com.nttdata.infrastructure.entrypoints.controllers.client;

import com.nttdata.domain.model.client.Client;
import com.nttdata.domain.usecases.client.ClientUseCase;
import com.nttdata.infrastructure.entrypoints.dto.MessageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/client", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ClientController {

    private final ClientUseCase clientUseCase;

    private static MessageResponse getMessageResponse(String message) {
        return MessageResponse.builder().date(LocalDateTime.now()).message(message).build();
    }

    @PostMapping
    public ResponseEntity<Object> created(@Validated @RequestBody Client client, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().body(getMessageResponse(result.getFieldError().getDefaultMessage()));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(clientUseCase.saveClient(client));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable("id") String id, @Valid @RequestBody Client client, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.badRequest().body(getMessageResponse(result.getFieldError().getDefaultMessage()));
        }
        return ResponseEntity.ok(clientUseCase.updateClient(id, client));
    }

    @DeleteMapping("/{id}")
    public MessageResponse delete(@Valid @PathVariable("id") String id) {
        String message = clientUseCase.deleteClient(id);
        return getMessageResponse(message);
    }

    @GetMapping
    public List<Client> findAll() {
        return clientUseCase.findAllClients();
    }

    @GetMapping("/{id}")
    public Client searchById(@PathVariable("id") String id) {
        return clientUseCase.searchClientById(id);
    }

    @GetMapping("/search")
    public Client searchByIdentificationTypeAndIdentificationNumber(@RequestParam(value = "tipoIdentificacion") String identificationType, @RequestParam(value = "numeroIdentificacion") String identificationNumber) {
        return clientUseCase.searchClientByIdentificationTypeAndIdentificationNumber(identificationType, identificationNumber);
    }

}
