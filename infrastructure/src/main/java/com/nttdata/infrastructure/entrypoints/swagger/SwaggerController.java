package com.nttdata.infrastructure.entrypoints.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ms-interview")
public class SwaggerController {

    @GetMapping("/swagger-ui.html")
    public String index() {
        return "forward:/swagger-ui.html";
    }

    @GetMapping("/webjars/springfox-swagger-ui/css/{s:.+}")
    public String css(@PathVariable String s) {
        return "forward:/webjars/springfox-swagger-ui/css/" + s;
    }

    @GetMapping("/webjars/springfox-swagger-ui/{s:.+}")
    public String baseJs(@PathVariable String s) {
        return "forward:/webjars/springfox-swagger-ui/" + s;
    }

    @GetMapping("/webjars/springfox-swagger-ui/lib/{s:.+}")
    public String js(@PathVariable String s) {
        return "forward:/webjars/springfox-swagger-ui/lib/" + s;
    }

    @GetMapping("/webjars/springfox-swagger-ui/images/{s:.+}")
    public String images(@PathVariable String s) {
        return "forward:/webjars/springfox-swagger-ui/images/" + s;
    }

    @GetMapping("/swagger-resources/configuration/ui")
    public String ui() {
        return "forward:/swagger-resources/configuration/ui";
    }

    @GetMapping("/swagger-resources")
    public String resources() {
        return "forward:/swagger-resources";
    }

    @GetMapping("/v2/api-docs")
    public String docs() {
        return "forward:/v2/api-docs";
    }

    @GetMapping("/swagger-resources/configuration/security")
    public String security() {
        return "forward:/swagger-resources/configuration/security";
    }
}