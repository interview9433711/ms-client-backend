package com.nttdata.infrastructure.entrypoints.error;

import com.nttdata.domain.usecases.exceptions.ClientBusinessException;
import com.nttdata.domain.usecases.exceptions.ClientNotFoundException;
import com.nttdata.domain.usecases.exceptions.IdentificationTypeNotFoundException;
import com.nttdata.infrastructure.entrypoints.dto.ResponseHandlerError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ControllerAdvisor {

    private static ResponseEntity<ResponseHandlerError> getResponseHandlerErrorResponseEntity(Integer code, Exception exception) {
        ResponseHandlerError handler = ResponseHandlerError.builder()
                .status(code)
                .message(exception.getMessage())
                .data(exception.getCause())
                .build();
        return new ResponseEntity<>(handler, HttpStatus.resolve(handler.getStatus()));
    }

    @ExceptionHandler(ClientBusinessException.class)
    public ResponseEntity<ResponseHandlerError> handlerError(ClientBusinessException exception) {
        log.error(exception.getMessage(), exception);
        return getResponseHandlerErrorResponseEntity(400, exception);
    }

    @ExceptionHandler(ClientNotFoundException.class)
    public ResponseEntity<ResponseHandlerError> handlerError(ClientNotFoundException exception) {
        log.error(exception.getMessage(), exception);
        return getResponseHandlerErrorResponseEntity(404, exception);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ResponseHandlerError> handlerError(MissingServletRequestParameterException exception) {
        log.error(exception.getMessage(), exception);
        return getResponseHandlerErrorResponseEntity(400, exception);
    }

    @ExceptionHandler(IdentificationTypeNotFoundException.class)
    public ResponseEntity<ResponseHandlerError> handlerError(IdentificationTypeNotFoundException exception) {
        log.error(exception.getMessage(), exception);
        return getResponseHandlerErrorResponseEntity(404, exception);
    }
}
