package com.nttdata.infrastructure.drivenadapters.jpa.client;

import com.nttdata.infrastructure.drivenadapters.jpa.identification.IdentificationTypeEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TB_CLIENT")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class ClientEntity {

    @Id
    @Column(name = "ID", nullable = false)
    private String identificationNumber;

    @NotNull
    @OneToOne
    @JoinColumn(name = "IDENTIFICATION_TYPE_ID", nullable = false)
    private IdentificationTypeEntity identificationType;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false, length = 50)
    private String firstName;

    @Column(name = "SECOND_NAME", length = 50)
    private String secondName;

    @NotNull
    @Column(name = "FIRST_SURNAME", nullable = false, length = 50)
    private String firstSurname;

    @Column(name = "SECOND_SURNAME", length = 50)
    private String secondSurname;

    @NotNull
    @Column(name = "PHONE", nullable = false, length = 10)
    private String phone;

    @NotNull
    @Column(name = "ADDRESS", nullable = false, length = 100)
    private String address;

    @NotNull
    @Column(name = "CITY", nullable = false, length = 100)
    private String city;

}
