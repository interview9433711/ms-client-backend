package com.nttdata.infrastructure.drivenadapters.mappers.client;

import com.nttdata.domain.model.client.Client;
import com.nttdata.infrastructure.drivenadapters.jpa.client.ClientEntity;
import lombok.Generated;

import java.util.Objects;

@Generated
public class ClientMapperImpl implements ClientMapper {

    @Override
    public ClientEntity dataToEntity(Client data) {
        if (Objects.isNull(data)) {
            return null;
        }
        return ClientEntity.builder()
                .identificationNumber(data.getIdentificationNumber())
                .firstName(data.getFirstName())
                .secondName(data.getSecondName())
                .firstSurname(data.getFirstSurname())
                .secondSurname(data.getSecondSurname())
                .phone(data.getPhone())
                .address(data.getAddress())
                .city(data.getCity())
                .build();
    }

    @Override
    public Client entityToData(ClientEntity entity) {
        if (Objects.isNull(entity)) {
            return null;
        }
        return Client.builder()
                .identificationNumber(entity.getIdentificationNumber())
                .identificationType(entity.getIdentificationType().getIdentificationCode())
                .firstName(entity.getFirstName())
                .secondName(entity.getSecondName())
                .firstSurname(entity.getFirstSurname())
                .secondSurname(entity.getSecondSurname())
                .phone(entity.getPhone())
                .address(entity.getAddress())
                .city(entity.getCity())
                .build();
    }
}
