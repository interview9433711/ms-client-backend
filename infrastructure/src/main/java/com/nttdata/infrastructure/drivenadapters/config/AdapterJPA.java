package com.nttdata.infrastructure.drivenadapters.config;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public abstract class AdapterJPA<E, D, I, R extends CrudRepository<E, I> & QueryByExampleExecutor<E>> {

    private final MapperAdapter<E, D> mapperAdapter;
    protected R repository;


    public AdapterJPA(R repository, MapperAdapter<E, D> mapperAdapter) {
        this.repository = repository;
        this.mapperAdapter = mapperAdapter;
    }

    public D save(D entity) {
        E entity1 = toEntity(entity);
        E saved = repository.save(entity1);
        return toData(saved);
    }

    public D findById(I id) {
        Optional<E> entity = repository.findById(id);
        return entity.map(this::toData).orElse(null);
    }

    public List<D> findAll() {
        Iterable<E> list = repository.findAll();
        List<D> aux = new ArrayList<>();
        if (list.iterator().hasNext()) {
            list.forEach(entity -> aux.add(toData(entity)));
        }
        return aux;
    }

    public void deleteById(I id) {
        repository.deleteById(id);
    }

    public E toEntity(D data) {
        return mapperAdapter.dataToEntity(data);
    }

    public D toData(E entity) {
        return mapperAdapter.entityToData(entity);
    }

    public D singleQuery(E query) {
        return toData(query);
    }

    public List<D> manyQuery(Supplier<Iterable<E>> query) {
        Iterable<E> list = query.get();
        List<D> aux = new ArrayList<>();
        if (list.iterator().hasNext()) {
            list.forEach(entity -> aux.add(toData(entity)));
        }
        return aux;
    }

}
