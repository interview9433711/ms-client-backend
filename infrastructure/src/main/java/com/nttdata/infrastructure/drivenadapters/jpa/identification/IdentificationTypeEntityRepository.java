package com.nttdata.infrastructure.drivenadapters.jpa.identification;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Optional;

public interface IdentificationTypeEntityRepository extends CrudRepository<IdentificationTypeEntity, Long>, QueryByExampleExecutor<IdentificationTypeEntity> {
    public Optional<IdentificationTypeEntity> findIdentificationTypeEntityByIdentificationCode(String code);
}

