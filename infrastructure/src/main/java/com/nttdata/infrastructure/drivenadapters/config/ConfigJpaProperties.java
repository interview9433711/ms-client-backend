package com.nttdata.infrastructure.drivenadapters.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "spring.jpa")
public class ConfigJpaProperties {
    private Map<String, String> hibernate = new HashMap<>();

    public Map<String, String> getHibernate() {
        return hibernate;
    }

    public void setHibernate(Map<String, String> hibernate) {
        final String HIBERNATE = "hibernate.";

        Map<String, String> fixProperties = new HashMap<>();
        for (Map.Entry<String, String> entry : hibernate.entrySet()) {
            fixProperties.put(HIBERNATE.concat(entry.getKey()), entry.getValue());
        }
        this.hibernate = fixProperties;
    }
}

