package com.nttdata.infrastructure.drivenadapters.mappers.identification;

import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.infrastructure.drivenadapters.jpa.identification.IdentificationTypeEntity;
import lombok.Generated;

import java.util.Objects;

@Generated
public class IdentificationTypeMapperImpl implements IdentificationTypeMapper {
    @Override
    public IdentificationTypeEntity dataToEntity(IdentificationType data) {
        if (Objects.isNull(data)) {
            return null;
        }
        return IdentificationTypeEntity.builder()
                .id(data.getId())
                .identificationCode(data.getIdentificationCode())
                .identificationName(data.getIdentificationName())
                .build();

    }

    @Override
    public IdentificationType entityToData(IdentificationTypeEntity entity) {
        if (Objects.isNull(entity)) {
            return null;
        }
        return IdentificationType.builder()
                .id(entity.getId())
                .identificationCode(entity.getIdentificationCode())
                .identificationName(entity.getIdentificationName())
                .build();
    }
}
