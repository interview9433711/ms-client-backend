package com.nttdata.infrastructure.drivenadapters.jpa.client;

import com.nttdata.domain.model.client.Client;
import com.nttdata.domain.model.gateway.ClientRepository;
import com.nttdata.infrastructure.drivenadapters.config.AdapterJPA;
import com.nttdata.infrastructure.drivenadapters.jpa.identification.IdentificationTypeEntity;
import com.nttdata.infrastructure.drivenadapters.jpa.identification.IdentificationTypeEntityRepository;
import com.nttdata.infrastructure.drivenadapters.mappers.client.ClientMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepositoryAdapter extends AdapterJPA<ClientEntity, Client, String, ClientEntityRepository> implements ClientRepository {

    private final IdentificationTypeEntityRepository identificationTypeEntityRepository;

    public ClientRepositoryAdapter(ClientEntityRepository repository, ClientMapper clientMapper, IdentificationTypeEntityRepository identificationTypeEntityRepository) {
        super(repository, clientMapper);
        this.identificationTypeEntityRepository = identificationTypeEntityRepository;
    }


    @Override
    public Client saveClient(Client client) {
        Optional<IdentificationTypeEntity> entity = identificationTypeEntityRepository
                .findIdentificationTypeEntityByIdentificationCode(client.getIdentificationType());
        if (entity.isEmpty()) {
            return null;
        }
        ClientEntity clientEntity = toEntity(client);
        clientEntity.setIdentificationType(entity.get());
        return toData(repository.save(clientEntity));
    }

    @Override
    public Client searchClientByIdentification(String identification) {
        return findById(identification);
    }

    @Override
    public boolean deleteClientByIdentification(String identification) {
        Optional<ClientEntity> entity = repository.findById(identification);
        if (entity.isPresent()) {
            deleteById(identification);
            return true;
        }
        return false;
    }

    @Override
    public Client updateClient(String identification, Client client) {
        Optional<ClientEntity> entity = repository.findById(identification);
        if (entity.isPresent()) {
            ClientEntity aux = entity.get();
            client.setIdentificationNumber(aux.getIdentificationNumber());
            return saveClient(client);
        }
        return null;
    }

    @Override
    public List<Client> findAllClients() {
        return findAll();
    }

    @Override
    public Client searchClientByIdentificationTypeAndIdentificationNumber(String identificationType, String identificationNumber) {
        Optional<ClientEntity> entity = repository.findClientEntityByIdentificationTypeIdentificationCodeAndIdentificationNumber(identificationType, identificationNumber);
        if (entity.isPresent()) {
            ClientEntity aux = entity.get();
            return toData(aux);
        }
        return null;
    }
}
