package com.nttdata.infrastructure.drivenadapters.jpa.client;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Optional;

public interface ClientEntityRepository extends CrudRepository<ClientEntity, String>, QueryByExampleExecutor<ClientEntity> {
    public Optional<ClientEntity> findClientEntityByIdentificationTypeIdentificationCodeAndIdentificationNumber(String identificationType, String identificationNumber);
}
