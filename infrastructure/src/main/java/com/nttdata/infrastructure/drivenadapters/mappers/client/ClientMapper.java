package com.nttdata.infrastructure.drivenadapters.mappers.client;

import com.nttdata.domain.model.client.Client;
import com.nttdata.infrastructure.drivenadapters.config.MapperAdapter;
import com.nttdata.infrastructure.drivenadapters.jpa.client.ClientEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ClientMapper extends MapperAdapter<ClientEntity, Client> {
}
