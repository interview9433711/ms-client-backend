package com.nttdata.infrastructure.drivenadapters.config;

import org.mapstruct.Mapper;

@Mapper
public interface MapperAdapter<E, D> {

    E dataToEntity(D data);

    D entityToData(E entity);

}
