package com.nttdata.infrastructure.drivenadapters.jpa.identification;

import com.nttdata.domain.model.gateway.IdentificationTypeRepository;
import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.infrastructure.drivenadapters.config.AdapterJPA;
import com.nttdata.infrastructure.drivenadapters.mappers.identification.IdentificationTypeMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class IdentificationTypeRepositoryAdapter extends AdapterJPA<IdentificationTypeEntity, IdentificationType, Long, IdentificationTypeEntityRepository> implements IdentificationTypeRepository {


    public IdentificationTypeRepositoryAdapter(IdentificationTypeEntityRepository repository, IdentificationTypeMapper identificationTypeMapper) {
        super(repository, identificationTypeMapper);
    }

    @Override
    public IdentificationType searchIdentificationTypeByCode(String code) {
        Optional<IdentificationTypeEntity> identificationType = repository.findIdentificationTypeEntityByIdentificationCode(code);
        return identificationType.map(this::singleQuery).orElse(null);
    }

    @Override
    public List<IdentificationType> finAllIdentificationType() {
        return findAll();
    }
}
