package com.nttdata.infrastructure.drivenadapters.jpa.identification;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TB_IDENTIFICATION_TYPE")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter(value = AccessLevel.PACKAGE)
@Getter
@ToString
public class IdentificationTypeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;


    @NotNull
    @Column(name = "CODE", nullable = false)
    private String identificationCode;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String identificationName;

}
