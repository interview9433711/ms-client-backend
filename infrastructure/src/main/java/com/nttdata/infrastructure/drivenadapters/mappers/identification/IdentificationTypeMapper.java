package com.nttdata.infrastructure.drivenadapters.mappers.identification;

import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.infrastructure.drivenadapters.config.MapperAdapter;
import com.nttdata.infrastructure.drivenadapters.jpa.identification.IdentificationTypeEntity;
import org.mapstruct.Mapper;

@Mapper
public interface IdentificationTypeMapper extends MapperAdapter<IdentificationTypeEntity, IdentificationType> {
}
