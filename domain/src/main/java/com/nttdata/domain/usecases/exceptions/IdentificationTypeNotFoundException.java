package com.nttdata.domain.usecases.exceptions;

public class IdentificationTypeNotFoundException extends RuntimeException {
    private String message;

    public IdentificationTypeNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
