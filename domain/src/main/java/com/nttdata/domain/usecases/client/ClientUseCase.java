package com.nttdata.domain.usecases.client;

import com.nttdata.domain.model.client.Client;
import com.nttdata.domain.model.gateway.ClientRepository;
import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.domain.usecases.exceptions.ClientBusinessException;
import com.nttdata.domain.usecases.exceptions.ClientNotFoundException;
import com.nttdata.domain.usecases.identification.IdentificationTypeUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

import static com.nttdata.domain.usecases.identification.IdentificationTypeUseCase.MESSAGE_INVALID_TYPE;


@Slf4j
@RequiredArgsConstructor
public class ClientUseCase {

    public static final String MESSAGE_DELETED_SUCCESS = "El cliente fue eliminado con exito";

    public static final String MESSAGE_DELETED_FAIL = "Ocurrio un error eliminando el cliente";

    public static final String MESSAGE_NOT_FOUND = "El cliente no fue encontrado o no existe";

    private final ClientRepository clientRepository;

    private final IdentificationTypeUseCase identificationTypeUseCase;

    public Client saveClient(Client client) {
        log.info("Saved client {}", client.toString());
        if (!validIdentificationType(client.getIdentificationType())) {
            log.error("identification type is invalid with code {}", client.getIdentificationType());
            throw new ClientBusinessException(MESSAGE_INVALID_TYPE);
        }
        return clientRepository.saveClient(client);
    }

    public Client updateClient(String idClient, Client client) throws ClientNotFoundException, ClientBusinessException {
        if (Objects.isNull(client) || !validIdentificationType(client.getIdentificationType())) {
            log.error("identification type is invalid with code or null");
            throw new ClientBusinessException(MESSAGE_INVALID_TYPE);
        }
        Client clientUpdated = clientRepository.updateClient(idClient, client);
        if (Objects.isNull(clientUpdated)) {
            log.error("Error in updated client by id {}", idClient);
            throw new ClientNotFoundException(MESSAGE_NOT_FOUND);
        }
        log.info("Updated client {}", client.toString());
        return client;
    }

    public String deleteClient(String idClient) throws ClientBusinessException {
        Client client1 = searchClientById(idClient);
        boolean deleted = clientRepository.deleteClientByIdentification(client1.getIdentificationNumber());
        if (!deleted) {
            log.error("Error in delete client by id {}", idClient);
            throw new ClientBusinessException(MESSAGE_DELETED_FAIL);
        }
        log.info("Deleted client by id {}", idClient);
        return MESSAGE_DELETED_SUCCESS;
    }

    public Client searchClientById(String idClient) throws ClientNotFoundException {
        log.info("Searched client by id {}", idClient);
        Client client = clientRepository.searchClientByIdentification(idClient);
        if (Objects.isNull(client)) {
            log.error("Client not found client by id {}", idClient);
            throw new ClientNotFoundException(MESSAGE_NOT_FOUND);
        }
        return client;
    }

    public Client searchClientByIdentificationTypeAndIdentificationNumber(String identificationType, String identificationNumber) throws ClientBusinessException, ClientNotFoundException {
        log.info("Searched client by identificationType {} and identification number {}", identificationType, identificationNumber);
        Client client = clientRepository.searchClientByIdentificationTypeAndIdentificationNumber(identificationType, identificationNumber);
        if (Objects.isNull(client)) {
            log.error("Client not found client by identificationType {} and identification number {}", identificationType, identificationNumber);
            throw new ClientNotFoundException(MESSAGE_NOT_FOUND);
        }
        return client;
    }

    public List<Client> findAllClients() {
        return clientRepository.findAllClients();
    }

    private boolean validIdentificationType(String code) {
        IdentificationType identificationType = identificationTypeUseCase.findByIdentificationCode(code);
        return !Objects.isNull(identificationType);
    }

}
