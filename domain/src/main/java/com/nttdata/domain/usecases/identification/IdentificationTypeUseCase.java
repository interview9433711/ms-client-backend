package com.nttdata.domain.usecases.identification;

import com.nttdata.domain.model.gateway.IdentificationTypeRepository;
import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.domain.usecases.exceptions.IdentificationTypeNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public class IdentificationTypeUseCase {

    public static final String MESSAGE_NOT_FOUND = "El tipo de identificacion no fue encontrado o no existe";
    public static final String MESSAGE_INVALID_TYPE = "El tipo de identificacion no es valido consulte tipo de identificacion soportados";
    private final IdentificationTypeRepository identificationTypeRepository;

    public IdentificationType findByIdentificationCode(String code) throws IdentificationTypeNotFoundException {
        IdentificationType identificationType = identificationTypeRepository.searchIdentificationTypeByCode(code);
        if (Objects.isNull(identificationType)) {
            log.error("the identificationType not was found by code {}", code);
            throw new IdentificationTypeNotFoundException(MESSAGE_NOT_FOUND);
        }
        log.info("get identificationType by code {}", code);
        return identificationType;
    }

    public List<IdentificationType> findAllIdentificationType() {
        return identificationTypeRepository.finAllIdentificationType();
    }


}
