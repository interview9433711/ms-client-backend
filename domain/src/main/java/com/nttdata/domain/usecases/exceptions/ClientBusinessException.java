package com.nttdata.domain.usecases.exceptions;

public class ClientBusinessException extends RuntimeException {

    private String message;

    public ClientBusinessException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
