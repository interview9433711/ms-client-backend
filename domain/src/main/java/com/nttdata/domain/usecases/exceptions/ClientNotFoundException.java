package com.nttdata.domain.usecases.exceptions;

public class ClientNotFoundException extends RuntimeException {
    private String message;

    public ClientNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
