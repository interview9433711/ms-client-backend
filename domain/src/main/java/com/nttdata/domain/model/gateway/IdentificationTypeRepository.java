package com.nttdata.domain.model.gateway;

import com.nttdata.domain.model.identification.IdentificationType;

import java.util.List;

public interface IdentificationTypeRepository {

    public IdentificationType searchIdentificationTypeByCode(String code);

    public List<IdentificationType> finAllIdentificationType();
}
