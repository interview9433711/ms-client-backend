package com.nttdata.domain.model.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@AllArgsConstructor
@ToString
public class Client {

    @JsonProperty(value = "numeroIdentificacion")
    @NotNull(message = "El campo numeroIdentificacion no debe ser nulo")
    @NotEmpty(message = "El campo numeroIdentificacion no debe estar vacio")
    private String identificationNumber;

    @JsonProperty(value = "tipoIdentificacion")
    @NotNull(message = "El campo tipoIdentificacion no debe ser nulo")
    @NotEmpty(message = "El campo tipoIdentificacion no debe estar vacio")
    private String identificationType;

    @JsonProperty(value = "primerNombre")
    @NotNull(message = "El campo primerNombre no debe ser nulo")
    @NotEmpty(message = "El campo primerNombre no debe estar vacio")
    private String firstName;

    @JsonProperty(value = "segundoNombre")
    private String secondName;

    @JsonProperty(value = "primerApellido")
    @NotNull(message = "El campo primerApellido no debe ser nulo")
    @NotEmpty(message = "El campo primerApellido no debe estar vacio")
    private String firstSurname;

    @JsonProperty(value = "segundoApellido")
    private String secondSurname;

    @JsonProperty(value = "telefono")
    @NotNull(message = "El campo telefono no debe ser nulo")
    @NotEmpty(message = "El campo telefono no debe estar vacio")
    @Length(min = 7, max = 10, message = "El campo telefono debe tener de 7 a 10 digitos")
    private String phone;

    @JsonProperty(value = "direccion")
    @NotNull(message = "El campo direccion no debe ser nulo")
    @NotEmpty(message = "El campo direccion no debe estar vacio")
    private String address;

    @JsonProperty(value = "ciudad")
    @NotNull(message = "El campo ciudad no debe ser nulo")
    @NotEmpty(message = "El campo ciudad no debe estar vacio")
    private String city;

}
