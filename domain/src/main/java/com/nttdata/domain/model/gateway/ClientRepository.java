package com.nttdata.domain.model.gateway;

import com.nttdata.domain.model.client.Client;

import java.util.List;

public interface ClientRepository {

    public Client saveClient(Client client);

    public Client searchClientByIdentification(String identification);

    public boolean deleteClientByIdentification(String identification);

    public Client updateClient(String identification, Client client);

    public List<Client> findAllClients();

    public Client searchClientByIdentificationTypeAndIdentificationNumber(String identificationType, String identificationNumber);

}
