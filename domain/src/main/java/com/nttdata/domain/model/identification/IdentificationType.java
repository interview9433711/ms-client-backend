package com.nttdata.domain.model.identification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@SuperBuilder
@AllArgsConstructor
@ToString
public class IdentificationType {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "codigotipoIdentificacion")
    @NotNull(message = "El campo numeroIdentificacion no debe ser nulo")
    @NotEmpty(message = "El campo numeroIdentificacion no debe estar vacio")
    private String identificationCode;

    @JsonProperty(value = "nombreTipoIdentificacion")
    @NotNull(message = "El campo numeroIdentificacion no debe ser nulo")
    @NotEmpty(message = "El campo numeroIdentificacion no debe estar vacio")
    private String identificationName;
}
