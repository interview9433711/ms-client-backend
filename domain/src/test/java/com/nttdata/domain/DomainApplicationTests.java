package com.nttdata.domain;

import com.nttdata.domain.model.client.Client;
import com.nttdata.domain.model.gateway.ClientRepository;
import com.nttdata.domain.model.identification.IdentificationType;
import com.nttdata.domain.usecases.client.ClientUseCase;
import com.nttdata.domain.usecases.exceptions.ClientBusinessException;
import com.nttdata.domain.usecases.exceptions.ClientNotFoundException;
import com.nttdata.domain.usecases.identification.IdentificationTypeUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DomainApplicationTests {


    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientUseCase clientUseCase;

    @Mock
    private IdentificationTypeUseCase identificationTypeUseCase;

    @Test
    @DisplayName("should save a client correctly")
    void mustSavedClient() {
        Client cl = getClient("123");
        IdentificationType identificationType = getIdentificationType("C");

        when(identificationTypeUseCase.findByIdentificationCode("C")).thenReturn(identificationType);
        when(clientRepository.saveClient(cl)).thenReturn(cl);

        Client client = clientUseCase.saveClient(cl);
        Assertions.assertNotNull(client);
        Assertions.assertEquals(cl.getIdentificationNumber(), client.getIdentificationNumber());

        verify(clientRepository, times(1)).saveClient(cl);
    }

    @Test
    @DisplayName("should update a client correctly")
    void mustUpdateClient() {
        Client cl = getClient("1234");
        IdentificationType identificationType = getIdentificationType("C");

        when(identificationTypeUseCase.findByIdentificationCode("C")).thenReturn(identificationType);
        when(clientRepository.updateClient("1234", cl)).thenReturn(cl);

        Client client = clientUseCase.updateClient("1234", cl);
        Assertions.assertNotNull(client);
        Assertions.assertEquals(cl.getIdentificationNumber(), client.getIdentificationNumber());

        verify(clientRepository, times(1)).updateClient("1234", cl);
    }

    @Test
    @DisplayName("shouldn't update a client correctly")
    void showExceptionInUpdateClientWhenClientNotExist() {
        Client cl = getClient("123");
        IdentificationType identificationType = getIdentificationType("C");

        when(identificationTypeUseCase.findByIdentificationCode("C")).thenReturn(identificationType);
        when(clientRepository.updateClient("123", cl)).thenReturn(null);

        Assertions.assertThrows(ClientNotFoundException.class, () -> {
            clientUseCase.updateClient("123", cl);
        });

        verify(clientRepository, times(1)).updateClient("123", cl);
    }

    @Test
    @DisplayName("should deleted a client correctly")
    void mustDeletedClient() {
        Client cl = getClient("1234");

        when(clientRepository.deleteClientByIdentification(cl.getIdentificationNumber())).thenReturn(true);
        when(clientRepository.searchClientByIdentification(cl.getIdentificationNumber())).thenReturn(cl);

        String value = clientUseCase.deleteClient("1234");
        Assertions.assertNotNull(value);
        Assertions.assertEquals(ClientUseCase.MESSAGE_DELETED_SUCCESS, value);

        verify(clientRepository, times(1)).deleteClientByIdentification("1234");
        verify(clientRepository, times(1)).searchClientByIdentification("1234");
    }

    @Test
    @DisplayName("shouldn't deleted a client correctly")
    void showExceptionInDeletedClientWhenClientExistButFail() {
        Client cl = getClient("123");

        when(clientRepository.deleteClientByIdentification(cl.getIdentificationNumber())).thenReturn(false);
        when(clientRepository.searchClientByIdentification(cl.getIdentificationNumber())).thenReturn(cl);

        Assertions.assertThrows(ClientBusinessException.class, () -> {
            clientUseCase.deleteClient("123");
        });

        verify(clientRepository, times(1)).deleteClientByIdentification("123");
        verify(clientRepository, times(1)).searchClientByIdentification("123");
    }

    @Test
    @DisplayName("shouldn't deleted a client correctly")
    void showExceptionInDeletedClientWhenClientNotExist() {
        Client cl = getClient("123");

        when(clientRepository.searchClientByIdentification(cl.getIdentificationNumber())).thenReturn(null);

        Assertions.assertThrows(ClientNotFoundException.class, () -> {
            clientUseCase.deleteClient("123");
        });

        verify(clientRepository, times(1)).searchClientByIdentification("123");
    }

    @Test
    @DisplayName("should return a client correctly")
    void mustReturnClient() {
        Client cl = getClient("1234");

        when(clientRepository.searchClientByIdentification(cl.getIdentificationNumber())).thenReturn(cl);

        Client client = clientUseCase.searchClientById("1234");
        Assertions.assertNotNull(client);
        Assertions.assertEquals(cl.getIdentificationNumber(), client.getIdentificationNumber());

        verify(clientRepository, times(1)).searchClientByIdentification("1234");
    }

    @Test
    @DisplayName("shouldn't return a client correctly")
    void showExceptionInSearchClientWhenClientNotExist() {
        Client cl = getClient("1234");

        when(clientRepository.searchClientByIdentification(cl.getIdentificationNumber())).thenReturn(null);

        Assertions.assertThrows(ClientNotFoundException.class, () -> {
            clientUseCase.searchClientById("1234");
        });

        verify(clientRepository, times(1)).searchClientByIdentification("1234");
    }

    @Test
    @DisplayName("should return a client correctly")
    void mustReturnListClients() {
        Client cl = getClient("1234");

        when(clientRepository.findAllClients()).thenReturn(Collections.singletonList(cl));

        List<Client> clientList = clientUseCase.findAllClients();

        Assertions.assertTrue(!clientList.isEmpty());
        Assertions.assertEquals(cl.getIdentificationNumber(), clientList.get(0).getIdentificationNumber());

        verify(clientRepository, times(1)).findAllClients();
    }

    @Test
    @DisplayName("shouldn't return a client correctly")
    void showReturnEmptyListClientsWhenNotExistClients() {
        Client cl = getClient("1234");

        when(clientRepository.findAllClients()).thenReturn(Collections.emptyList());

        List<Client> clientList = clientUseCase.findAllClients();

        Assertions.assertNotNull(clientList);
        Assertions.assertTrue(clientList.isEmpty());

        verify(clientRepository, times(1)).findAllClients();
    }

    private Client getClient(String id) {
        return Client.builder()
                .identificationNumber(id)
                .identificationType("C")
                .build();
    }

    private IdentificationType getIdentificationType(String code) {
        return IdentificationType.builder()
                .identificationCode(code)
                .build();
    }

}
