# MS-BACKEND

Microservicio basado en una arquitectura hexagonal en lenguaje en java, maven y el framework de springboot version 2.7.9
y JDk 11

## Requierimientos

Tener Java version 11 o superior instalada

## Ejecutar

En el editor de preferencia ejecutar el boot main Application ubicado en la siguiente ruta

```bash
RUTA: src/main/java/com/nttdata/Application.java
```

## Usage

Importar el archivo adjunto en la raiz del repositorio INSOMNIA.json en el cliente Insomnia

![Swagger UI](data/images/img_1.png)

ó usar la vista integrada de swagger en la siguiente ruta

```bash
RUTA: http://localhost:8090/ms-interview/swagger-ui.html
```

![Swagger UI](data/images/img_2.png)
